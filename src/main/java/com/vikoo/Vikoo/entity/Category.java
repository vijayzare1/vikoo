package com.vikoo.Vikoo.entity;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private Long quantity;
	private String imgUrl;
	private Date createdAt = Date.from(Instant.now());
	private Date updateddAt;
	private boolean deletedAt = Boolean.FALSE;
	
	
	@OneToMany(mappedBy = "category",cascade = CascadeType.ALL)
	private List<Product> products;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Long getQuantity() {
		return quantity;
	}


	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}


	public String getImgUrl() {
		return imgUrl;
	}


	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdateddAt() {
		return updateddAt;
	}


	public void setUpdateddAt(Date updateddAt) {
		this.updateddAt = updateddAt;
	}


	public boolean isDeletedAt() {
		return deletedAt;
	}


	public void setDeletedAt(boolean deletedAt) {
		this.deletedAt = deletedAt;
	}


	public List<Product> getProducts() {
		return products;
	}


	public void setProducts(List<Product> products) {
		this.products = products;
	}


	public Category(Long id, String name, Long quantity, String imgUrl, Date createdAt, Date updateddAt,
			boolean deletedAt, List<Product> products) {
		super();
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.imgUrl = imgUrl;
		this.createdAt = createdAt;
		this.updateddAt = updateddAt;
		this.deletedAt = deletedAt;
		this.products = products;
	}
	public Category() {
		// TODO Auto-generated constructor stub
	}
	
}
