package com.vikoo.Vikoo.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vikoo.Vikoo.entity.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {

	@Query(value = "select * from product p  where p.name= :pw", nativeQuery = true)
	List<Product> getBuyQuery(String pw);
	
	@Query(value = "select * from product p  where p.product_cart_id= :productId", nativeQuery = true)
	Optional<Product> findByProductId(Long productId);
 
}
