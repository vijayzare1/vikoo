package com.vikoo.Vikoo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vikoo.Vikoo.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
