package com.vikoo.Vikoo.Repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import com.vikoo.Vikoo.entity.Category;

public class CategorySpecification{
	private static final String discountField = "name";
	@Autowired
	private EntityManager entityManager;

	public List<Category> withDynamicQuery(String search) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery(Category.class);

		Root<Category> category = cq.from(Category.class);

		Predicate firstNamePredicate = cb.equal(category.get("name"), search);
		Predicate departmentPredicate = cb.equal(category.get("products").get("name"), search);

		cq.where(firstNamePredicate, departmentPredicate);

		TypedQuery<Category> query = entityManager.createQuery(cq);

		return  query.getResultList();
	}

	
}
