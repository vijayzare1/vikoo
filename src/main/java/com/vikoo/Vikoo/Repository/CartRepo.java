package com.vikoo.Vikoo.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vikoo.Vikoo.entity.Cart;
import com.vikoo.Vikoo.entity.Product;

public interface CartRepo extends JpaRepository<Cart, Long> {

	@Query(value = "select * from cart c  where c.user_cart_id= :productId", nativeQuery = true)
	Optional<Product> findByProductId(Long productId);

	@Query(value = "select * from cart c  where c.user_cart_id= :userId", nativeQuery = true)
	List<Cart> findByUserId(Long userId);
	
	@Query(value = "select * from cart c  where c.user_cart_id= :userId and c.product_cart_id= :productId ", nativeQuery = true)
	Optional<Cart> findByUserIdAndProduct(Long userId, Long productId);

//	List<Cart> findByIdAndUserCartId(Long userId, Long cartId);

}
