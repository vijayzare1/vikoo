package com.vikoo.Vikoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VikooApplication {

	public static void main(String[] args) {
		SpringApplication.run(VikooApplication.class, args);
	}

}
