package com.vikoo.Vikoo.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vikoo.Vikoo.Repository.UserRepository;
import com.vikoo.Vikoo.entity.User;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/{id}")
	public Optional<User> getUserBuyId(@PathVariable Long id) {
		return userRepository.findById(id);
	}

	@GetMapping
	public List<User> getUserBuyId() {
		return userRepository.findAll();
	}
	
	@PostMapping
	public User addUser(@RequestBody User user) {
		return userRepository.save(user);
	}
	
	
	
	
	
	
	
	
}
