package com.vikoo.Vikoo.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vikoo.Vikoo.Repository.CartRepo;
import com.vikoo.Vikoo.Repository.ProductRepo;
import com.vikoo.Vikoo.Repository.UserRepository;
import com.vikoo.Vikoo.entity.Cart;
import com.vikoo.Vikoo.entity.Product;
import com.vikoo.Vikoo.entity.User;
import com.vikoo.Vikoo.model.ApiResponse;

@RestController
@RequestMapping("/cart")
public class CartController {

	@Autowired
	private CartRepo cartRepository;
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ProductRepo productRepo;

	@GetMapping("/user/{userId}")
	private List<Cart> getItemsForUser(@PathVariable Long userId) {
		return cartRepository.findByUserId(userId);

	}

	@GetMapping
	private List<Cart> getAllCart() {
		return cartRepository.findAll();
	}

	@GetMapping("/cartId")
	private Optional<Cart> getCart(@PathVariable Long cartId) {
		return cartRepository.findById(cartId);
	}
	
	@Transactional
	@PostMapping("/{userId}/{productId}")
	private ApiResponse AddToCart(@PathVariable Long userId, @PathVariable Long productId) {
		Cart cart = new Cart();
		Optional<Cart> cartExist = cartRepository.findByUserIdAndProduct(userId, productId);

		if (cartExist.isPresent()) {
			System.out.println("allredy exuist");
			int quantity = cartExist.get().getQuantity();
			int incQntVal = quantity + 1;
			cartExist.get().setQuantity(incQntVal);

			return new ApiResponse(HttpStatus.ALREADY_REPORTED, "alredy exist.. incrising quantity",
					cartRepository.save(cart));
		}

		User user = userRepository.findById(userId).get();
		Product product = productRepo.findById(productId).get();
		cart.setProduct(product);
		cart.setUser(user);

		return new ApiResponse(HttpStatus.CREATED, "added to cart ", cartRepository.save(cart));
	}

	@DeleteMapping("/delete/{userId}/{cartId}")
	private ApiResponse removeFromCart(@PathVariable Long cartId, @PathVariable Long userId) {

		Optional<Cart> cartExist = cartRepository.findById(cartId);
		if (cartExist.isPresent()) {
			int qty = cartExist.get().getQuantity();
			int modifiedQty = 0;
			if (qty > 1) {
				modifiedQty = qty - 1;

			}
			cartExist.get().setQuantity(modifiedQty);
			
			return new ApiResponse(HttpStatus.OK, "alredy exist.. dicrising  quantity", cartRepository.save(cartExist.get()));
		}
		
		cartRepository.deleteById(cartId);
		return new ApiResponse(HttpStatus.OK, "product deleted form cart");

	}

}
