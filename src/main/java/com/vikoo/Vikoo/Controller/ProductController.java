package com.vikoo.Vikoo.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vikoo.Vikoo.Repository.CategoryRepo;
import com.vikoo.Vikoo.Repository.ProductRepo;
import com.vikoo.Vikoo.entity.Product;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ProductRepo productRepo;
	
	
	@Autowired
	private CategoryRepo categoryRepo;
	
	@GetMapping
	public List<Product> getAllProduct() {
		
		return productRepo.findAll();	
		}

	@PostMapping("/{catId}")
	public Optional<Object> AddProductToCategory(@RequestBody Product dto,@PathVariable Long catId) {
		
		return categoryRepo.findById(catId).map(category -> {
		dto.setCategory(category);
		return productRepo.save(dto);
		});
	//	git clone https://vijayzare1@bitbucket.org/komalzare1/vikoo.git
	}
	
	@PutMapping("/{id}")
	public Product updateById(@RequestBody Product product,@PathVariable Long id) {
		
		Product product2 = productRepo.findById(id).get();
		product2.setName(product.getName());
		product2.setPrize(product.getPrize());
		product2.setImgUrl(product.getImgUrl());
		
		return productRepo.save(product2);
	}
	
	@DeleteMapping("/{id}")
	public List<Product> deleteById(@RequestBody Long id) {
		
		productRepo.deleteById(id);
		return productRepo.findAll();
	}
	
	@GetMapping("/query")
	public List<Product> forNativeQuery() {
		String pw  = "samsung";
	return	productRepo.getBuyQuery(pw);
	}
	
	
}
