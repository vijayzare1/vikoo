package com.vikoo.Vikoo.Controller;

import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vikoo.Vikoo.Repository.CategoryRepo;
import com.vikoo.Vikoo.entity.Category;
import com.vikoo.Vikoo.model.ApiResponse;

@RestController
@RequestMapping("/cat")
public class CategoryController {

	@Autowired
	private CategoryRepo categoryRepo;

	@GetMapping
	public List<Category> getAllCategory() {
		return categoryRepo.findAll();
	}
// 54UVwbz5eQw2eMeVyhGg
	// pagination impl
	@GetMapping("/page")
	public ApiResponse getCatPage(@RequestParam Map<String, String> param) {
		Integer page = Integer.parseInt(param.get("page"));
		Integer size = Integer.parseInt(param.get("size"));
		Pageable pagable = PageRequest.of(page, size);
		Page<Category> catPage = categoryRepo.findAll(pagable);

	// 	return catPage;
		return new ApiResponse(HttpStatus.OK, "dataFound", catPage);

	}

	@PostMapping
	public Category AddCategory(@RequestBody Category dto) {

		return categoryRepo.save(dto);
	}

	@PutMapping("/{id}")
	public Category updateById(@RequestBody Category category, @PathVariable Long id) {

		Category category2 = categoryRepo.findById(id).get();

		category2.setName(category.getName());
		category2.setQuantity(category.getQuantity());
		category2.setImgUrl(category.getImgUrl());

		return categoryRepo.save(category2);
	}

	@DeleteMapping("/{id}")
	public List<Category> deleteCategory(@PathVariable Long id) {

		categoryRepo.deleteById(id);
		return categoryRepo.findAll();
	}
}
